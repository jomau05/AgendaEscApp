﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgendaEscApp
{
    using Xamarin.Forms;
    using AgendaEscApp.View;
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            //MainPage = new AgendaEscApp.MainPage();
            MainPage = new NavigationPage(new LoginPage());
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
