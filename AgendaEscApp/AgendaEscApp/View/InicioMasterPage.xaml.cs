﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AgendaEscApp.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InicioMasterPage : MasterDetailPage
	{
		public InicioMasterPage ()
		{
			InitializeComponent ();
            masterPage.ListaMenu.ItemSelected += LstMenuItems_ItemSelected;
		}

        private void LstMenuItems_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as Model.MasterMenuItem;
            if (item == null)
                return;
            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Titulo;
            Detail = new NavigationPage(page);
            IsPresented = false;
            masterPage.ListaMenu.SelectedItem = null;
        }
    }
}