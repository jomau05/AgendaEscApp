﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AgendaEscApp.View.Master
{
    using AgendaEscApp.ModelView;
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPage : ContentPage
	{
		public MasterPage ()
		{
			InitializeComponent ();
            this.BindingContext = new MasterPageModelView();
            ListaMenu = LstMenuItems;

        }

        #region Propiedades
        public ListView ListaMenu { get; set; }
        #endregion
    }
}