﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AgendaEscApp.View
{
    using AgendaEscApp.ModelView;
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NotificacionesPage : ContentPage
	{
		public NotificacionesPage ()
		{
			InitializeComponent ();
            this.BindingContext = new NotificacionesPageModelView();
		}
	}
}