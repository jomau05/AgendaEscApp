﻿
namespace AgendaEscApp.ModelView
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    public class Notificable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName=null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
