﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgendaEscApp.ModelView
{
    using System.Collections.ObjectModel;
    using AgendaEscApp.View;
    using AgendaEscApp.Model;

    public class MasterPageModelView : Notificable
    {
        #region Atributos
        private ObservableCollection<MasterMenuItem> menuItems;
        private int num;
        #endregion
        #region Constructor
        public MasterPageModelView()
        {
            MenuItems = new ObservableCollection<MasterMenuItem>(new [] {
                new MasterMenuItem{ Titulo="Inicio", Icono="home.png", TargetType= typeof(InicioPage)},
                new MasterMenuItem{ Titulo="Notificaciones", Icono="notificaciones.png",TargetType= typeof(NotificacionesPage)},
                new MasterMenuItem{ Titulo="Tareas", Icono="menu.png",TargetType= typeof(TareasPage)},
                new MasterMenuItem{ Titulo="Calendario", Icono="menu.png",TargetType= typeof(CalendarioPage)},
                new MasterMenuItem{ Titulo="Usuarios", Icono="user.png",TargetType= typeof(UsuariosPage)}
            });
            num = MenuItems.Count;
        }
        #endregion
        #region Propiedades
        public ObservableCollection<MasterMenuItem> MenuItems
        {
            get { return menuItems; }
            set
            {
                if (menuItems != value)
                {
                    menuItems = value;
                    OnPropertyChanged();
                }
            }
        }
        #endregion

    }
}
