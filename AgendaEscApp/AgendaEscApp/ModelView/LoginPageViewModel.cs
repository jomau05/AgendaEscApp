﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgendaEscApp.ModelView
{
    using System.Threading.Tasks;
    using Xamarin.Forms;
    
    public class LoginPageViewModel : Notificable
    {
        #region Atributos
        private string usrTxt;
        private string pwdTxt;
        private bool isEnabled;
        #endregion
        #region Contructores
        public LoginPageViewModel()
        {
            IngresarBtnCommand = new Command((x) => IngresarBtn());
        }
        #endregion
        #region Propiedades
        public string UsrTXt
        {
            get { return usrTxt; }
            set
            {
                if (usrTxt != null)
                {
                    usrTxt = value;
                    OnPropertyChanged();
                }
            }
        }

        public string PwdTXt
        {
            get { return pwdTxt; }
            set
            {
                if (pwdTxt != null)
                {
                    pwdTxt = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    OnPropertyChanged();
                }
            }
        }

        public Command IngresarBtnCommand { get; set; }
        public INavigation Navigation { get; set; }
        #endregion
        #region Métodos
        private async void IngresarBtn()
        {
            if (!IsEnabled)
            {
                IsEnabled = true;
                await Task.Delay(3000);
                await Navigation.PushModalAsync(new AgendaEscApp.View.InicioMasterPage());
                IsEnabled = false;

            }
        }
        #endregion
    }
}
