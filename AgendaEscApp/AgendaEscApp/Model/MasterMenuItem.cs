﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AgendaEscApp.Model
{
    public class MasterMenuItem
    {
        #region Propiedades
        public string Titulo { get; set; }
        public string Icono { get; set; }
        public Type TargetType { get; set; } 
        #endregion
    }
}
